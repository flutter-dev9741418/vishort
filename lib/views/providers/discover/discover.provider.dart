import 'package:flutter/material.dart';
import 'package:vishort/domain/entities/video_post.entity.dart';
import 'package:vishort/domain/repositories/video_posts.repository.dart';

class DiscoverProvider extends ChangeNotifier {
  final VideoPostsRepository videoPostsRepository;

  bool loading = true;
  List<VideoPostEntity> videos = [];

  DiscoverProvider({
    required this.videoPostsRepository,
  });

  Future<void> loadVideos() async {
    final newVideos = await videoPostsRepository.getTredingVideosByPage(1);

    videos.addAll(newVideos);
    loading = false;
    notifyListeners();
  }
}
