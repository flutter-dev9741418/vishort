import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:vishort/shared/widgets/video_scrollable_view.widget.dart';
import 'package:vishort/views/providers/discover/discover.provider.dart';

class DiscoverScreen extends StatelessWidget {
  const DiscoverScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final discoverProvider = context.watch<DiscoverProvider>();

    return SafeArea(
      child: Scaffold(
        body: discoverProvider.loading
            ? const Center(
                child: CircularProgressIndicator(
                  strokeWidth: 2,
                ),
              )
            : VideoScrollableViewWidget(videos: discoverProvider.videos),
      ),
    );
  }
}
