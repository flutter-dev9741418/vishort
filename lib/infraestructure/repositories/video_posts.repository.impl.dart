import 'package:vishort/domain/entities/video_post.entity.dart';
import 'package:vishort/domain/repositories/video_posts.repository.dart';
import 'package:vishort/domain/datasource/video_posts.datasource.dart';

class VideoPostsReposityImpl implements VideoPostsRepository {
  final VideoPostsDatasource videosPostsDatasource;

  VideoPostsReposityImpl({
    required this.videosPostsDatasource,
  });

  @override
  Future<List<VideoPostEntity>> getFavoriteVideosByUser(int idUser) {
    throw UnimplementedError();
  }

  @override
  Future<List<VideoPostEntity>> getTredingVideosByPage(int page) {
    return videosPostsDatasource.getTredingVideosByPage(page);
  }
}
