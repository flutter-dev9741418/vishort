import 'package:vishort/domain/datasource/video_posts.datasource.dart';
import 'package:vishort/domain/entities/video_post.entity.dart';
import 'package:vishort/infraestructure/models/local_video.model.dart';
import 'package:vishort/shared/data/local_video_post.data.dart';

class LocalVideoPostsDatasourceImpl implements VideoPostsDatasource {
  @override
  Future<List<VideoPostEntity>> getFavoriteVideosByUser(int idUser) {
    throw UnimplementedError();
  }

  @override
  Future<List<VideoPostEntity>> getTredingVideosByPage(int page) async {
    final List<VideoPostEntity> newVideos = videoPosts
        .map((e) => LocalVideoModel.fromJson(e).toVideoPostEntity())
        .toList();

    return newVideos;
  }
}
