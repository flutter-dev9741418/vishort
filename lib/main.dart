import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:vishort/config/themes/app.theme.dart';
import 'package:vishort/infraestructure/datasource/local_video_posts.datasource.impl.dart';
import 'package:vishort/views/providers/discover/discover.provider.dart';
import 'package:vishort/views/screens/discover/discover.screen.dart';
import 'package:vishort/infraestructure/repositories/video_posts.repository.impl.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    final videoPostsRepository = VideoPostsReposityImpl(
        videosPostsDatasource: LocalVideoPostsDatasourceImpl());

    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          lazy: false,
          create: (_) =>
              DiscoverProvider(videoPostsRepository: videoPostsRepository)
                ..loadVideos(),
        )
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'ViShort',
        theme: AppTheme(selectColor: 1, mode: Brightness.dark).theme(),
        home: const DiscoverScreen(),
      ),
    );
  }
}
