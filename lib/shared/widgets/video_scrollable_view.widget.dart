import 'package:flutter/material.dart';
import 'package:vishort/domain/entities/video_post.entity.dart';
import 'package:vishort/shared/widgets/video/fullscreen_player.widget.dart';
import 'package:vishort/shared/widgets/video_buttons.widget.dart';

class VideoScrollableViewWidget extends StatelessWidget {
  final List<VideoPostEntity> videos;

  const VideoScrollableViewWidget({
    super.key,
    required this.videos,
  });

  @override
  Widget build(BuildContext context) {
    return PageView.builder(
      physics: const BouncingScrollPhysics(),
      scrollDirection: Axis.vertical,
      itemCount: videos.length,
      itemBuilder: (context, index) {
        final video = videos[index];

        return Stack(
          children: [
            SizedBox.expand(
              child: FullScreenPlayerWidget(
                videoUrl: video.videoUrl,
                caption: video.caption,
              ),
            ),
            Positioned(
              right: 10,
              bottom: 25,
              child: VideoButtonsWidget(video: video),
            ),
          ],
        );
      },
    );
  }
}
