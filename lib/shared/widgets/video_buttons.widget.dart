import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:vishort/config/helpers/human_format.helper.dart';
import 'package:vishort/domain/entities/video_post.entity.dart';

class VideoButtonsWidget extends StatelessWidget {
  final VideoPostEntity video;

  const VideoButtonsWidget({
    super.key,
    required this.video,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        _CustomIconButton(
          value: video.likes,
          icon: Icons.favorite,
          iconColor: Colors.redAccent,
          onClick: () {},
        ),
        const SizedBox(
          height: 10,
        ),
        _CustomIconButton(
          value: video.views,
          icon: Icons.remove_red_eye_outlined,
          onClick: () {},
        ),
        const SizedBox(
          height: 10,
        ),
        SpinPerfect(
          infinite: true,
          duration: const Duration(seconds: 4),
          child: _CustomIconButton(
            value: null,
            icon: Icons.play_circle_outline,
            onClick: () {},
          ),
        ),
      ],
    );
  }
}

class _CustomIconButton extends StatelessWidget {
  final int? value;
  final IconData icon;
  final Color color;
  final VoidCallback? onClick;

  const _CustomIconButton({
    required this.value,
    required this.icon,
    required this.onClick,
    iconColor,
  }) : color = iconColor ?? Colors.white;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        IconButton(
          onPressed: onClick,
          icon: Icon(
            icon,
            color: color,
          ),
        ),
        if (value != null)
          Text(HumanFormatHelper.humanReadbleNumber(value!.toDouble()))
      ],
    );
  }
}
