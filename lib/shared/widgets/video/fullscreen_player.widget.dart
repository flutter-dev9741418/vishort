import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';
import 'package:vishort/shared/widgets/video/video_background.widget.dart';

class FullScreenPlayerWidget extends StatefulWidget {
  final String videoUrl;
  final String caption;

  const FullScreenPlayerWidget({
    super.key,
    required this.videoUrl,
    required this.caption,
  });

  @override
  State<FullScreenPlayerWidget> createState() => _FullScreenPlayerWidgetState();
}

class _FullScreenPlayerWidgetState extends State<FullScreenPlayerWidget> {
  late VideoPlayerController controller;

  @override
  void initState() {
    super.initState();

    controller = VideoPlayerController.asset(widget.videoUrl)
      ..setVolume(0)
      ..setLooping(true)
      ..play();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: controller.initialize(),
      builder: (context, snapshot) {
        return snapshot.connectionState == ConnectionState.done
            ? GestureDetector(
                onTap: () {
                  controller.value.isPlaying
                      ? controller.pause()
                      : controller.play();
                },
                child: AspectRatio(
                  aspectRatio: controller.value.aspectRatio,
                  child: Stack(
                    children: [
                      VideoPlayer(controller),
                      VideoBackgroundWidget(stops: const [0.4, 1.0]),
                      Positioned(
                        left: 15,
                        bottom: 40,
                        child: _VideoCaptionWidget(caption: widget.caption),
                      ),
                    ],
                  ),
                ),
              )
            : const Center(
                child: CircularProgressIndicator(
                  strokeWidth: 2,
                ),
              );
      },
    );
  }
}

class _VideoCaptionWidget extends StatelessWidget {
  final String caption;

  const _VideoCaptionWidget({required this.caption});

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final titleStyle = Theme.of(context).textTheme.titleLarge;

    return SizedBox(
      width: size.width * 0.6,
      child: Text(
        caption,
        maxLines: 2,
        style: titleStyle,
      ),
    );
  }
}
