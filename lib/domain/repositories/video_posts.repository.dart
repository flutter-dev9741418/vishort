import 'package:vishort/domain/entities/video_post.entity.dart';

abstract class VideoPostsRepository {
  Future<List<VideoPostEntity>> getTredingVideosByPage(int page);
  Future<List<VideoPostEntity>> getFavoriteVideosByUser(int idUser);
}
